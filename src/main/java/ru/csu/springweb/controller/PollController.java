package ru.csu.springweb.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.csu.springweb.dto.Poll;
import ru.csu.springweb.service.PollService;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/polls")
public class PollController {

    private final PollService pollService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public void createPoll(@RequestBody @Valid Poll poll) {
        pollService.createPoll(poll);
    }

    @GetMapping("/{id}")
    public ru.csu.springweb.model.Poll getPollById(@PathVariable long id) {
        return pollService.getPoll(id);
    }
}
